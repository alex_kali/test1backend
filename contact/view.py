from django.contrib import auth
from rest_framework import permissions, status
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from contact.models import Contact
from contact.serializer import ContactSerializer
from user.serializer import UserSerializer

class ContactsView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    def post(self, request,):
        name = request.data.get("name")
        phone = request.data.get("phone")
        if(request.user.is_authenticated):
            contact = Contact.objects.create(user=request.user,name=name, phone=phone)
            contact = ContactSerializer(contact).data
            return Response(contact)
        else:
            return Response({'username': 'AnonymousUser'}, status=status.HTTP_401_UNAUTHORIZED)
    def get(self, request,):
        if (request.user.is_authenticated):
            contacts = Contact.objects.filter(user=request.user)
            contacts = ContactSerializer(contacts, many=True).data
            return Response(contacts)

        else:
            return Response({'username': 'AnonymousUser'}, status=status.HTTP_401_UNAUTHORIZED)

class ContactView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    def delete(self, request, pk):
        if (request.user.is_authenticated):
            contact = Contact.objects.filter(id=pk, user=request.user)
            contact.delete()
            return Response({'status': 'success', 'id': pk})
        else:
            return Response({'username': 'AnonymousUser'}, status=status.HTTP_401_UNAUTHORIZED)
    def put(self, request, pk):
        name = request.data.get("name")
        phone = request.data.get("phone")
        if (request.user.is_authenticated):
            contact = Contact.objects.filter(id=pk, user=request.user)
            contact.update(name=name, phone=phone)
            contact = contact.get()
            contact = ContactSerializer(contact).data
            return Response(contact)
        else:
            return Response({'username': 'AnonymousUser'}, status=status.HTTP_401_UNAUTHORIZED)
