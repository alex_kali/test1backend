from django.db import models
from rest_framework.authtoken.admin import User


class Contact(models.Model):
    class Meta():
        db_table = 'Contact'
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=40)
    name = models.CharField(max_length=40)