from rest_framework import serializers


class ContactSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=200)
    phone = serializers.CharField(max_length=200)