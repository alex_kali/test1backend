from django.urls import path, include

from contact.view import ContactsView, ContactView

urlpatterns = [
    path('', ContactsView.as_view()),
    path('<pk>/', ContactView.as_view()),
]