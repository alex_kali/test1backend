from rest_framework import routers
from django.urls import path, include

from user.view import LoginView, GetUserView, LogoutView, RegistrationView

urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("registration/", RegistrationView.as_view(), name="user_create"),
    path('get_user_data/', GetUserView.as_view()),
]