from django.contrib import auth
from django.contrib.auth import authenticate, login, logout
from rest_framework import viewsets, generics, permissions, status, authentication
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.authtoken.admin import User
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from user.serializer import UserSerializer


class LoginView(APIView):
    permission_classes = ()
    def post(self, request,):
        username = request.data.get("username")
        password = request.data.get("password")
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            user_data = UserSerializer(user).data
            return Response(user_data)
        else:
            return Response({"error": "Wrong Credentials"}, status=status.HTTP_400_BAD_REQUEST)

class LogoutView(APIView):
    permission_classes = ()
    def get(self, request,):
        logout(request)
        return Response({})


class RegistrationView(APIView):
    permission_classes = ()
    def post(self, request,):
        username = request.data.get("username")
        email = request.data.get("email")
        password = request.data.get("password")
        user = User.objects.create_user(username=username,
                                        email=email,
                                        password=password)
        Token.objects.create(user=user)
        login(request, user)
        user_data = UserSerializer(user).data
        return Response(user_data)

class GetUserView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    def get(self, request,):
        user = request.user
        if (request.user.is_authenticated):
            user_data = UserSerializer(user).data
            return Response(user_data)
        else:
            return Response({'username': 'AnonymousUser'}, status=status.HTTP_401_UNAUTHORIZED)

