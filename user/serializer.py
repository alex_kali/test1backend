from django.contrib.auth import login
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.authtoken.models import Token


class UserSerializer(serializers.ModelSerializer):


    class Meta:
        model = User
        fields = ('username', 'email', 'token')

    token = serializers.SerializerMethodField()

    def get_token(self, obj):
        token = Token.objects.get(user=obj)
        return token.key


